package principal;
import java.util.Locale;
import java.util.Scanner;
import entidades.Medidas;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		Medidas medidas;
		
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		System.out.println();
		System.out.print("Sobrenome: ");
		String sobrenome = sc.nextLine();
		System.out.print("Digite o peso: ");
		double peso = sc.nextDouble();
		System.out.print("Digite a altura:");
		double altura = sc.nextDouble();
		
		medidas = new Medidas(nome, sobrenome, peso, altura);
		
		medidas.calcImc(peso, altura);
		System.out.print(medidas);
		
		
		System.out.println();
		//System.out.print(medidas.getAltura() / medidas.getPeso());
		//System.out.print(medidas.getPeso());
		
		sc.close();

	}

}
