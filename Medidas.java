package entidades;

public class Medidas {
	
	private String nome;
	private String sobrenome;
	private double altura;
	private double peso;
	private double imc;
	
	public Medidas(String nome, String sobrenome, double peso, double altura) {
		this.nome = nome;
		this.sobrenome = sobrenome;		
		this.peso = peso;
		this.altura = altura;
	}
	
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}	

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	
	public double getImc() {
		return imc;
	}

	public void setImc(double imc) {
		this.imc = imc;
	}
	
	public double calcImc(double peso, double altura) {
		return imc = peso / Math.pow(altura, 2);			
	}
	
	/*public void calcImc(double calcImc) {
		calcImc = peso / altura * altura;
		imc = calcImc;
	}*/

	public String toString() {
		return "Nome:" 
				+ nome.toUpperCase()
				+ " "  
				+ sobrenome.toUpperCase()
				+ " " 
				+ String.format("%.2f", imc);
	}
	
	

}
